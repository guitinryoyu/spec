# Smart Language Filters (SLF)

- SLF should be enabled by default
    - If user disabled notifications (Show useful notifications), don't show SLF notification
    - Don't show this feature to DE/FR language users and don't enable by default
- Notify (with icon animation) user that ABP noticed that user visited a language page that has no relevant language filter list installed.
    - Notify the user only if there is an avaible relevant language filter list 
    - If language list already installed - don't notify user
    - If user has 3 or more language filter lists already installed - don't notify user
- This feature should be availble for desktop users only.
  
### Notification for new languages

| Item | Text |
| --- | --- |
| Description | `It looks like you visited a website in: \[INSERT LANGUAGE\]. Would you like to block ads on all websites in this language?` `Yes` |

- `Yes` - Open [Add filter subscription dialogue](desktop-settings/advanced-tab.md#add-filter-subscription-dialogue) for language filter list and dismiss message
- If user closes the notification, don't show it in the future again for the same language only.
 
	
## SLF Disabled

- When user un-check/disable the feature, stop adding new languages and notifying the user
- Don’t remove languages that were already installed

## Language detection

Website language detection will be done using a combination of at least one explicit and one implicit method and not to do anything unless both agree to minimize false positives.

| # | Type | Description | Source |
| --- | --- | --- | --- |
| 1 | Explicit | Document language | `<html lang>` |
| 2 | Implicit | Language detector | `browser.tabs.detectLanguage()` |
